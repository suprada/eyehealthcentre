// paths
var paths =  {
	html : {
		src: ["src/**/*.html"],
		dest: "build"
	},
	javascript: {
		src: ["src/js/**/*.js"],
		dest: "build/js"
	},
	css: {
		src: ["src/styles/**/*.css"],
		dest: "build/css"
	},
	bower: {
		src: "bower_components",
		dest: "build/lib"
	},
	fonts: {
		src: ["src/fonts/**/*.{eot,svg,ttf,woff,woff2}",
		"bower_components/font-awesome/fonts/*.{eot,svg,ttf,woff,woff2}"
					],
		dest: "build/fonts"
	},
	images: {
		src: "src/assets/images/",
		dest: "build/assets/images/"
	},
	sass: {
		src: ["src/styles/**/*.scss"],
		dest: "build/css"
	}
};

var images = [
	 { folder: 'topbar', width: 440, crop: false },
	 { folder: 'landingPage', width: 2000, crop: false},
	 { folder: 'patientStories', width: 600, crop: false},
	 { folder: 'services', width: 400, crop: false},
	 { folder: 'about', width:340, crop: false}
];

var gulp = require("gulp");
var glob = require("glob");
var sourcemaps = require("gulp-sourcemaps");
var concat = require("gulp-concat");
var uglify = require("gulp-uglify");
var minifyHTML = require("gulp-minify-html");
var inject = require("gulp-inject");
var cssmin = require("gulp-cssmin");
var uncss = require("gulp-uncss");
var browserSync = require("browser-sync");
//bower components
var mainBowerFiles = require("main-bower-files");
//for clean up
var del = require("del");
var rename = require('gulp-rename');
//for images
var imageresize = require('gulp-image-resize');
var imagemin = require('gulp-imagemin');
var pngquant = require('gulp-pngquant');
//sass
var sass = require('gulp-sass');
var merge = require('merge-stream');


//minify html
gulp.task("html", function(){
	//var sources = gulp.src('build/**/*.css', {read: false, addRootSlash: false, ignorePath:'/build/'});

	return gulp.src(paths.html.src)
		//inject app css files from dest
		.pipe(inject(
			gulp.src([paths.css.dest+"**/*.css"], {read:false}),
			{starttag: ' <!-- inject:app:css -->'
				, ignorePath: "build"
				, addRootSlash: false}
			))
		//inject app js
		.pipe(inject(
			gulp.src([paths.javascript.dest+"**/*.js"], {read:false}),
			{starttag: ' <!-- inject:app:js -->'
				, ignorePath: "build"
				, addRootSlash: false}
			))
		// inject bower dependecies
		.pipe(inject(
			gulp.src(
					mainBowerFiles(),
					{read: false, cwd: "bower_components"}
				),
			{name: "bower"
				, addPrefix: "lib"
				, addRootSlash: false
				, ignorePath: "../bower_components/"
				, relative: true
			}
			))
		.pipe(minifyHTML())
		.pipe(gulp.dest(paths.html.dest));
});

// minify scripts
gulp.task("scripts", function(){
	return gulp.src(paths.javascript.src)
		.pipe(sourcemaps.init())
		.pipe(concat("app.js"))
		.pipe(uglify())
		.pipe(rename({suffix: '.min'}))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(paths.javascript.dest));
});

// minify css
// gulp.task("css", function() {
// 	return gulp.src(paths.css.src)
// 		.pipe(sourcemaps.init())
		// .pipe(cssmin())
		// .pipe(rename({suffix: '.min'}))
// 		.pipe(sourcemaps.write("."))
// 		.pipe(gulp.dest(paths.css.dest))
// 		.pipe(browserSync.reload({stream:true}));
// });

// vendor sass files
// gulp.task("vendorcss", function(){
// 	return gulp.src("")
// });

//sass
// gulp.task("sass", function(){
// 	return gulp.src(paths.sass.src)
// 		.pipe(sourcemaps.init())
// 		.pipe(sass({
// 			includePaths: ['./bower_components/bootstrap-sass/assets/stylesheets',
// 											'./bower_components/font-awesome/scss']
// 		}).on('error', sass.logError))
// 		.pipe(sourcemaps.write("."))
// 		.pipe(gulp.dest(paths.sass.dest));
// });

//merge sass and css files into one
gulp.task("styles", function(){
	sassStream = gulp.src(paths.sass.src)
		.pipe(sourcemaps.init())
		.pipe(sass({
			includePaths: ['./bower_components/bootstrap-sass/assets/stylesheets',
											'./bower_components/font-awesome/scss']
		}).on('error', sass.logError))
		.pipe(sourcemaps.write("."))
		// .pipe(gulp.dest(paths.sass.dest));


	cssStream = gulp.src(paths.css.src)
		.pipe(sourcemaps.init())
		// .pipe(cssmin())
		// .pipe(rename({suffix: '.min'}))
		.pipe(sourcemaps.write("."));
		// .pipe(gulp.dest(paths.css.dest));


		return merge(sassStream,cssStream)
			.pipe(concat('app.css'))
			.pipe(gulp.dest(paths.css.dest))
			.pipe(browserSync.reload({stream:true}));
})

//browser-sync
gulp.task("browser-sync", function(){
	browserSync({
		server: {
			baseDir: "./build"
		}

	});
});

//bower
gulp.task("bower", function() {
	return gulp.src(mainBowerFiles(),
			{base: "bower_components"})
		.pipe(gulp.dest(paths.bower.dest));
});

//fonts
gulp.task("fonts", function(){
	return gulp.src(paths.fonts.src)
		.pipe(gulp.dest(paths.fonts.dest));
});

//images
// from https://gist.github.com/ryantbrown/239dfdad465ce4932c81
gulp.task("images", function() {
	//loop through image groups
	images.forEach(function(type){
		//build the resize object
		var resize_settings = {
			width: type.width,
			crop: type.crop,
			// never increase image dimensions
			upscale: false
		}

		//only specify the height if it exists
		if (type.hasOwnProperty("height")){
			resize_settings.height = type.height
		}

		//grab all images from the folder
		gulp.src(paths.images.src+type.folder+'/**/*' )
			//resize according to width/height settings
			.pipe(imageresize(resize_settings))
			//optimize images
			.pipe(imagemin({
				progressive: true,
				//set this if you are using svg images
				svgoPlugins: [{removeViewBox: false}],
				use: [pngquant()]
			}))
			// output each image to the dest path
			// maintaining the folder structure
			.pipe(gulp.dest(paths.images.dest+type.folder));
	});
});

// Clean images Task
gulp.task('clean:images', function () {
    return del(paths.images.dest);
});

// Clean script Task
gulp.task('clean:script', function () {
    return del(paths.javascript.dest);
});

// Clean fonts Task
gulp.task('clean:fonts', function () {
    return del(paths.fonts.dest);
});

// Clean styles Task
gulp.task('clean:styles', function () {
    return del(paths.css.dest);
});

//Clean lib Task
gulp.task('clean:bower', function(){
	return del(paths.bower.dest);
});

gulp.task("build", ["clean", "bower", "fonts", "styles", "scripts", "images"], function(){
	gulp.run("html");
});

gulp.task('clean', ['clean:images', 'clean:script', 'clean:styles']);

gulp.task("default", ["build", "browser-sync"], function(){
	gulp.watch(paths.html.src, ["html", browserSync.reload]);
	gulp.watch(paths.javascript.src, ["scripts", browserSync.reload]);
	gulp.watch(paths.bower.src, ["bower", browserSync.reload]);
	gulp.watch(paths.images.src+'**/*.*', ["images", browserSync.reload]);
	gulp.watch(paths.css.src, ["styles", browserSync.reload]);
	gulp.watch(paths.sass.src, ["styles", browserSync.reload]);
});

# README #

This README would normally document whatever steps are necessary to get your application up and running.


### How do I get set up? ###

1. **Clone this repo **

        git clone https://suprada@bitbucket.org/suprada/eyehealthcentre.git
        cd EyeHealthCentre

 2. **Acquire build dependencies.** Make sure you have [Node.js](http://nodejs.org/) installed on your workstation. This is only needed to _build_ from sources. The app itself has no dependency on Node.js once it is built (it works with any server technology or none). Now run:

        npm install 

    The first `npm` command sets up the popular [Grunt](http://gruntjs.com/) build tool. You might need to run this command with `sudo` if you're on Linux or Mac OS X, or in an Administrator command prompt on Windows. The second `npm` command fetches the remaining build dependencies.

 3. **Install the dependencies**

        npm install
        npm install gulp -g
        npm install -g graceful-fs graceful-fs@latest

        bower install 

4. To build, open a browser and run on the command prompt:
       
        gulp


* Suprada Urval
* Other community or team contact



### Troubleshoting: ###

1. When running gulp, if  you see "Error: "Could not execute GraphicsMagick/ImageMagick: identify "-ping" "-format" "%wx%h" "-" this most likely means the gm/convert binaries can't be found" 

Try:
On Mac:
 brew install imagemagick
 brew install graphicsmagick

On Linux:
  sudo apt install graphicsmagick